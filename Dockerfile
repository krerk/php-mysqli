FROM php:7.3-apache-bullseye
label MAINTAINER="Krerk Piromsopa, PH.D. <Krerk.P@chula.ac.th>"

       
#RUN docker-php-ext-configure gd --with-jpeg-dir=/usr/include/
RUN docker-php-ext-install mysqli
#RUN cp /etc/ssl/openssl.cnf /tmp/openssl.cnf
#RUN sed 's/^CipherString = DEFAULT@SECLEVEL=2/#&/' /tmp/openssl.cnf >/etc/ssl/openssl.cnf
